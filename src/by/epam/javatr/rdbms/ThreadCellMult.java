package by.epam.javatr.rdbms;

public class ThreadCellMult extends Thread{
	private int[][] source1;
	private int[][] source2;
	private int pointCell1;
	private int pointCell2;
	
	public ThreadCellMult(int[][] s1, int[][] s2, int k1, int k2) {
		this.source1 = s1;
		this.source2 = s2;
		pointCell1 = k1;
		pointCell2 = k2;
		this.setName(String.format("thread that multiplay %3s on%3s",k1,k2));
	}
	
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t.getName()+ " started " +(++ProcessMultiplMatrix.g)+" threads launched now");
		Integer result = 0;
		for (int i = 0; i < source1.length; i++){
			result = result + source1[pointCell1][i] * source2[i][pointCell2];
		}
		System.out.println(t.getName()+ " estimated " + result+" "+ProcessMultiplMatrix.g+" threads launched now");
		ProcessMultiplMatrix.result[pointCell1][pointCell2] = result;
		System.out.println(t.getName()+" passed result to matrix"+" "+(--ProcessMultiplMatrix.g)+" threads launched now\n");
	}

}
