package by.epam.javatr.rdbms;

import java.util.ArrayList;

public class ProcessMultiplMatrix {
	//let it be like counter for all launched threads.
	public static volatile int  g=0; 
	public static int[][] result = null;
	private static int dimens=5;
	private static int[][] src1 ={
			{0, 8, 7, 6, 5},
			{9, 8, 7, 6, 5},
			{19, 38, 27, 36, 15},
			{9, 8, 7, 6, 5},
			{9, 8, 7, 6, 5}
	};
	private static int[][] src2 ={
				{1, 2, 23, 4, 5},
				{1, 2, 33, 4, 5},
				{1, 2, 63, 4, 5},
				{1, 2, 13, 4, 5},
				{1, 2, 3, 4, 5}
		};

	/***
	 * output matrix
	 * @param one of a source or result matrix
	 */
	private static void printIt(int[][] tbl){
		for (int i = 0; i < tbl.length; i++){
			for (int j = 0; j < tbl.length; j++){
				System.out.printf("%7s",tbl[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		ArrayList<ThreadCellMult> threadsList = new ArrayList<ThreadCellMult>();
		printIt(src1);
		printIt(src2);
		result = new int[dimens][dimens];
		// create threads  
		for (int i = 0; i < dimens; i++){
			for (int j = 0; j < dimens; j++){
				threadsList.add(new  ThreadCellMult(src1, src2,i,j));
			}
		}
		// try to start them at the same time 
		for (ThreadCellMult t:threadsList) {
			t.start();
		}
		
		// wait for all been done 
		for (ThreadCellMult t : threadsList){
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("So, finally "+ProcessMultiplMatrix.g+" threads launched now");
		printIt(result);
	}

}
